<?php
require('smarty/libs/Smarty.class.php');

header("Content-Type:text/html; charset=utf-8");
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "train5";

$conn = mysqli_connect($servername, $username, $password);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

mysqli_query($conn,"set character set utf8");
mysqli_query($conn,"SET CHARACTER_SET_database= utf8");
mysqli_query($conn,"SET CHARACTER_SET_CLIENT= utf8");
mysqli_query($conn,"SET CHARACTER_SET_RESULTS= utf8");

mysqli_query($conn,'SET CHARACTER SET utf8');
mysqli_query($conn,"SET collation_connection = utf8_general_ci");

mysqli_select_db($conn,"train5");

$sql = "SELECT * FROM customer";
$result = mysqli_query($conn, $sql);


//顯示
while($row = mysqli_fetch_array($result)){
    $list[]=array("id"=>$row['id'], "name"=>$row['name'], "userid"=>$row['userid'], "birthday"=>$row['birthday'], "phone"=>$row['phone'], "code"=>$row['code'], "address"=>$row['address']);
}

//新增
if(isset($_POST["add"])){
    $insert = "INSERT INTO `customer` (`id`, `name`, `userid`, `birthday`, `phone`, `code`, `address`) VALUES (NULL, '$_POST[name]', '$_POST[userid]', '$_POST[birthday]', '$_POST[phone]', '$_POST[code]', '$_POST[address]')";

    if ($conn->query($insert) === TRUE) {
        echo "";
    } else {
        echo "Error: " . $insert . "<br>" . $conn->error;
    }
    header("location:train5.php");
}

//修改
if(isset($_POST["update"])){
    $update = "UPDATE `customer` SET `name` = '$_POST[name]', `userid` = '$_POST[userid]', `birthday` = '$_POST[birthday]', `phone` = '$_POST[phone]', `code` = '$_POST[code]', `address` = '$_POST[address]' WHERE `customer`.`id` = $_POST[id]";

    if ($conn->query($update) === TRUE) {
        echo "";
    } else {
        echo "Error: " . $update . "<br>" . $conn->error;
    }
    header("location:train5.php");
}

//刪除
if(isset($_POST["delete"])){
    $delete = "DELETE FROM `customer` WHERE `customer`.`id` = $_POST[id]";

    if ($conn->query($delete) === TRUE) {
        echo "";
    } else {
        echo "Error: " . $delete . "<br>" . $conn->error;
    }
    header("location:train5.php");
}

//檢查身分證
if(isset($_POST["check"])){
    if(strlen($_POST["userid"]) != 10 ){
        echo "有誤"."<br>";
        echo "<br>";
    }


    $head = array('A'=>10,'B'=>11,'C'=>12,'D'=>13,'E'=>14,'F'=>15,
        'G'=>16,'H'=>17,'I'=>34,'J'=>18,'K'=>19,'L'=>20,
        'M'=>21,'N'=>22,'O'=>35,'P'=>23,'Q'=>24,'R'=>25,
        'S'=>26,'T'=>27,'U'=>28,'V'=>29,'W'=>32,'X'=>30,
        'Y'=>31,'Z'=>33);

    if(mb_ereg("^[A-Za-z][1-2][0-9]{8}$",$_POST["userid"])){ //檢查格式
        for($i=0;$i<10;$i++){
            $idarray[$i] = substr($_POST["userid"],$i,1);
        }
        $idarray[0] = strtoupper($idarray[0]); //小寫變大寫

        $a[0] = substr($head[$idarray[0]],0,1);
        $a[1] = substr($head[$idarray[0]],1,1);
        $total = $a[0]*1 + $a[1]*9;
        for($j=1;$j<=8;$j++){
            $total = $total + $idarray[$j]*(9-$j);
        }

        if($total%10 == 0){
            $checksum = 0;
        }
        else{
            $checksum = 10 - $total%10;
        }
        if($idarray[9] == $checksum){
            echo "正確<br>";
        }
        else{
            echo "錯誤<br>";
        }
    }
    else{
        echo "錯誤<br>";
    }

}





$conn->close();




$smarty=new Smarty();
$smarty->template_dir='d:/xampp/htdocs/xampp/templates'; //指定模板存放目錄
$smarty->config_dir='d:/xampp/htdocs/xampp/config';//指定配置文件目錄
$smarty->cache_dir='d:/xampp/htdocs/xampp/smarty/cache';//指定緩存目錄
$smarty->compile_dir='d:/xampp/htdocs/xampp/smarty/templates_c';//指定編譯後的模板目錄
$smarty->assign("list",$list);

$smarty->display('index.tpl');
