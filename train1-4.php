<table cellpadding="5" cellspacing="5">

<?php
echo "<tr>";
for($x=2;$x<=5;$x++){
	echo "<td>";
	for($y=1;$y<=9;$y++){
		echo $x."x".$y."=".$x*$y."<br>";
		}
		echo "</td>";	
}
echo "</tr>";
echo "<tr>";
for($i=6;$i<=9;$i++){	
	echo "<td>";
	for($j=1;$j<=9;$j++){
		echo $i."x".$j."=".$i*$j."<br>";
		}
		echo "</td>";		
}
echo "</tr>";

//while
$i=2;
$j=1;
echo "<tr>";
while($i<6){
    echo "<td>";
    while($j<10){
        echo $i."x".$j."=".$i*$j."<br>";
        $j++;
    }
    echo "</td>";
    $i++;
    $j=1;
}
echo "</tr>";
$ii=6;
$jj=1;
echo "<tr>";
while($ii<10){
    echo "<td>";
    while($jj<10){
        echo $ii."x".$jj."=".$ii*$jj."<br>";
        $jj++;
    }
    echo "</td>";
    $ii++;
    $jj=1;
}
echo "</tr>";

//do while
$a=2;
$b=1;
echo "<tr>";
do{
    echo "<td>";
    do{
        echo $a."x".$b."=".$a*$b."<br>";
        $b++;
    }while($b<10);
    echo "</td>";
    $a++;
    $b=1;
}while($a<6);
echo "</tr>";
echo "<tr>";
$aa=6;
$bb=1;
do{
    echo "<td>";
    do{
        echo $aa."x".$bb."=".$aa*$bb."<br>";
        $bb++;
    }while($bb<10);
    echo "</td>";
    $aa++;
    $bb=1;
}while($aa<10);


echo "</tr>";
?>
</table>