<?php

function get_day( $num ){
    $tem = explode('-' , $num);       //切割日期,得到年份和月份
    $year = $tem['0'];
    $month = $tem['1'];
    if( in_array($month , array( 1 , 3 , 5 , 7 , 8 , 10 , 12))){
        // 月有31天
        $text = '31';
    }
    elseif( $month == 2 ){
        if ( $year%400 == 0  || ($year%4 == 0 && $year%100 !== 0) ){     //閏年判斷
            $text = '29';
        }
        else{
            $text = '28';
        }
    }
    else{
        $text = '30';
    }
    return $text;
}
//當月天數
$day = get_day($_POST["year"].'-'.$_POST["month"]);


echo "<h1>{$_POST["year"]}年{$_POST["month"]}月</h1>";

//星期幾
$days = array( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ); //12個月
$days[1] +=  $_POST["year"]%400 == 0  || ($_POST["year"]%4 == 0 && $_POST["year"]%100 !== 0) ;
$monthday = 1; //月第一天
$d = $_POST["day"];
for( $i=0 ; $i < $_POST["month"]-1 ; $i++){ //今年第幾天
    $d += $days[$i];
}
$_POST["year"]--;
$d += $_POST["year"] + ( $_POST["year"]/4 ) - ( $_POST["year"]/100 ) + ( $_POST["year"]/400 ); //加總總天數
$d = $d % 7 ;

//echo $d;
//echo "<br>";

//當月第一天星期幾
for( $i=0 ; $i < $_POST["month"]-1 ; $i++){ //今年第幾天
    $monthday += $days[$i];
}
$monthday += $_POST["year"] + ( $_POST["year"]/4 ) - ( $_POST["year"]/100 ) + ( $_POST["year"]/400 ); //加總總天數
$monthday = $monthday % 7 ;
//echo $monthday;




//月曆1
echo "<table width='600' border='1'>";
echo "<tr>";
echo "<th>星期日</th>";
echo "<th>星期一</th>";
echo "<th>星期二</th>";
echo "<th>星期三</th>";
echo "<th>星期四</th>";
echo "<th>星期五</th>";
echo "<th>星期六</th>";
echo "</tr>";

$dd=1;
while($dd<=$day){
    echo "<tr>";
        for($i=0;$i<7;$i++){
            if(($monthday>$i && $dd==1) || $dd>$day){
                echo "<td> </td>";
            }
            else{
                if($dd == $_POST["day"]){
                    echo "<td style='color:#ff0000'>{$dd}</td>";
                    $dd++;
                }
                 else{
                     echo "<td>{$dd}</td>";
                     $dd++;
                 }

            }

        }
    echo "</tr>";
}
echo "</table>";

//月曆二
echo "<br>";
echo "<table width='600' border='1'>";
echo "<tr>";
echo "<th>星期一</th>";
echo "<th>星期二</th>";
echo "<th>星期三</th>";
echo "<th>星期四</th>";
echo "<th>星期五</th>";
echo "<th>星期六</th>";
echo "<th>星期日</th>";
echo "</tr>";

$dd=1;
while($dd<=$day){
    $monthday = ($monthday + 6) %7;
    echo "<tr>";
    for($i=0;$i<7;$i++){

        if(($monthday>$i && $dd==1) || $dd>$day){
            echo "<td> </td>";
        }
        else{
            if($dd == $_POST["day"]){
                echo "<td style='color:#ff0000'>{$dd}</td>";
                $dd++;
            }
            else{
                echo "<td>{$dd}</td>";
                $dd++;
            }

        }

    }
    echo "</tr>";
}

echo "</table>";
?>